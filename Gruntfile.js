module.exports = function(grunt) {
    // load all grunt tasks matching the ['grunt-*', '@*/grunt-*'] patterns
    require('load-grunt-tasks')(grunt);
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            css: {
                files: ['src/css/*.css'],
                tasks: ['cssmin'],
            },
            scripts: {
                files: ['src/js/*.js', 'libs/**/*.js', 'Gruntfile.js'],
                tasks: ['concat', 'uglify'],
            }
        },

        cssmin: {
            my_target: {
                files: [{
                    expand: true,
                    cwd: 'build/css/',
                    src: ['*.css', '!*.min.css'],
                    build: 'build/css/',
                    ext: '.min.css'
                }]
            }
        },

        concat: {
            options: {
                seperator: "\n\n",
                sourceMap: true,
                //stripeBanners: true,
                //banner: '/*!<%= pkg.name %> - v<%= pkg.version %> - ' + ' <%= grunt.template.today("yyyy-mm-dd") %> */',
            },
            dist: {
                src: ['src/js/plugin.js'],
                dest: 'build/js/plugin.js',
            },
            /* deps: {
                src: [
                    'libs/jquery/dist/jquery.js',
               ],
                build: 'build/js/app-deps.js'
            }, */
            // move: {
            //     src: ['libs/angularjs/angular.min.js.map'],
            //     build: 'build/js/angular.min.js.map'
            // }
        },
        uglify: {
            options: {
                manage: false,
                sourceMap: true,
                sourceMapIncludeSources: true,
                /*For presreve comments i minified file*/
                preserveComments: 'all'
            },
            // Following task will take all the js in "build/js" folder and combine in one minifyed js
            minify_all_js: {
                files: {
                    'build/js/plugin.min.js': ['build/js/plugin.js']
                }
            },
        },
        browserSync: {
            dev: {
                bsFiles: {
                    src: ['build/css/*.css', 'build/*.html', 'build/js/*.js']
                },
                options: {
                    watchTask: true,
                    server: './build'
                }
            }
        },
    });
    grunt.registerTask('default', ['browserSync', 'watch']);
};
